const qs = require('qs');
import axios from 'axios';
import TileResponseInterface from "./interfaces/TileResponseInterface";

export const Query = {
    /** Fetches tiles from the endpoint and coverts them to the schema structure */
    tiles: (parent: any, {pageNumber = 1, pageSize = 10}: any) => {

        const params = {
            page: {
                size: pageSize,
                number: pageNumber
            }
        };

        const queryParams = qs.stringify(params);

        const url = `${process.env.LAYBUY_API_ENDPOINT}/tiles?${queryParams}`;

        return axios.get<TileResponseInterface>(url).then(({ data: body}) => {

            return body.data.map( tile => {
                const { id, attributes } = tile;

                return {
                    id,
                    ...attributes,
                    tileImage: attributes.tileImage.url
                }
            });
        });
    },
};
