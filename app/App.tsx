import React from 'react';
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from '@apollo/react-hooks';
import {StyleSheet, Platform, StatusBar, View} from 'react-native';
import getEnvVars from './variables';

import HomeScreen from "./screens/HomeScreen";

export const link = createHttpLink({
    uri: getEnvVars.apiUrl
});

export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link,
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

export default function App() {
  return (
      <ApolloProvider client={client}>
          <View style={styles.container}>
              {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
              <HomeScreen/>
          </View>
      </ApolloProvider>
  );
}