import { TileItem, TileInterface} from "./TileItem";
import ListFooter from "./ListFooter";
import React  from "react";
import InfiniteScroll from "react-infinite-scroll-component";

export interface TileList {
    tiles: Array<TileInterface>,
    loadTiles: () => void
}

export default function TileListWeb({ tiles, loadTiles}: TileList) {
    return (
        <InfiniteScroll
            dataLength={tiles.length}
            next={() => loadTiles()}
            hasMore={true}
            loader={<ListFooter/>}>
            { tiles.map( (item ) => <TileItem tile={item} key={item.id} />)}
        </InfiniteScroll>
    );
}