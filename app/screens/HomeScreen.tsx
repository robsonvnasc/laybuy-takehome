import { SafeAreaView, StyleSheet, Text, Platform } from "react-native";
import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import TileListWeb from "../components/TileListWeb";
import TileListNative from "../components/TileListNative";
import {TileInterface} from "../components/TileItem";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
});

const pageSize = 4;


interface ApolloQueryResponseInterface {
    tiles: Array<TileInterface>
}

export default function HomeScreen() {

    const [ pageNumber, setPageNumber ] = useState(1);

    const { loading, error, data , fetchMore } = useQuery(GET_TILES, {
        variables: {
            pageNumber: 1,
            pageSize
        },
    });

    const loadTiles = () => {

        fetchMore({
            variables: {
                pageNumber: pageNumber + 1,
                pageSize,
            },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult) return prev;

                return Object.assign({}, prev, {
                    tiles: [...prev.tiles , ...fetchMoreResult.tiles]
                });
            }
        });

        setPageNumber(pageNumber + 1 );
    };

    if (loading) return <Text>Loading</Text>;

    if (error) return <Text>An error occurred when fetching the data.</Text>;


    const  { tiles = [] } : ApolloQueryResponseInterface = data || {};

    const renderTileList = () => {

        if (Platform.OS === 'web') {
            return <TileListWeb tiles={tiles} loadTiles={loadTiles}/>;
        }

        return  <TileListNative pageSize={pageNumber} tiles={tiles} loadTiles={loadTiles}/>
    };

    return (
        <SafeAreaView style={styles.container}>
            { renderTileList() }
        </SafeAreaView>
    );
}


const GET_TILES = gql`
    query getTiles($pageNumber: Int $pageSize: Int) {
        tiles(pageNumber: $pageNumber  pageSize: $pageSize) {
            id,
            name,
            tileImage,
            url
        }
    }
`;
